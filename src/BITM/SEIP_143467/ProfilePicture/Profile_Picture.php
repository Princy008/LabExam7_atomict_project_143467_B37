<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
//include("Database.php");
class Profile_Picture extends DB
{
    public $id = "";
    public $user_name= "";
    public $profile_picture = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    }
    public function  setData($data=NULL)
    {
        if (array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }
        if (array_key_exists('user_name',$data))
        {

            $this->user_name=$data['user_name'];
        }
        if (array_key_exists('profile_picture',$data))
        {

            $this->profile_picture=$data['profile_picture'];
        }
    }
    public function store()
    {
        $arrData=array($this->user_name,$this->profile_picture);


        $sql="insert into profile_picture(user_name,profile_picture)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [ProfilePicture: $this->profile_picture ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [ProfilePicture: $this->profile_picture ] <br> Data Has not  Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }

}